package CuatroEnRaya;

import javax.swing.*;
import java.awt.*;
import java.lang.*;
import java.awt.event.*;

public class CuatroEnRaya extends JFrame implements ActionListener
{
        int turno=0;
        boolean fin;
        boolean dosJugadores;

        //Botones

        RoundButton boton[][]=new RoundButton[7][7];

        //Menus
        JMenuBar Barra=new JMenuBar();
        JMenu Opciones=new JMenu("Opciones");
        JMenuItem Nuevo=new JMenuItem("Nuevo");
        JMenuItem Salir=new JMenuItem("Salir");

        JLabel Nombre=new JLabel("Cuatro en Raya",JLabel.CENTER);

        CuatroEnRaya()
        {
            //menu
            Nuevo.addActionListener(this);
            Opciones.add(Nuevo);
            Opciones.addSeparator();
            Salir.addActionListener(this);
            Opciones.add(Salir);
            Barra.add(Opciones);
            setJMenuBar(Barra);


            //Panel Principal

            JPanel Principal=new JPanel();
            Principal.setLayout(new GridLayout(7,7));

            //Colocar

            for(int fila=0;fila<7;fila++)
            {
                for(int col=0;col<7;col++)
                {
                    boton[fila][col]=new RoundButton();
                    if(fila==0)
                    {
                        boton[fila][col].setText("V");
                        boton[fila][col].setFont(new Font("Arial",Font.BOLD,50));
                        boton[fila][col].addActionListener(this);
                        boton[fila][col].setBackground(Color.ORANGE);
                    }
                    else
                    {
                        boton[fila][col].setBackground(Color.WHITE);
                        boton[fila][col].setText(null);
                        boton[fila][col].setFont(new Font("Arial",Font.BOLD,50));
                    }
                    Principal.add(boton[fila][col]);
                }
                Nombre.setForeground(Color.BLUE);
                add(Nombre,"North");
                add(Principal,"Center");

            }

            //Para cerrar la Ventana

            addWindowListener(new WindowAdapter()
            {
                public void windowClosing(WindowEvent we)
                {
                    System.exit(0);
                }
            });

            //Tamaño de la fuente

            setLocation(170,25);
            setSize (600,600);
            setResizable(true);
            setTitle("Cuatro en Raya");
            setVisible(true);

        }
        void ganar(int x,int y)
        {

           //x fila
           //y columna

            fin=false;

            //Quien gana en horizontal

            int ganar1=0;
            int ganar2=0;
            for(int col=0;col<7;col++)
            {
                if(boton[y][col].getText()!=null)
                {
                    if(boton[y][col].getText().equals("X"))
                    {
                        ganar1++;
                    }
                    else ganar1=0;
                    if(ganar1==4)
                    {
                        JOptionPane.showMessageDialog(this,"Gana el jugador que va con el color rojo","ENHORABUENA",JOptionPane.INFORMATION_MESSAGE);
                        VolverEmpezar();
                        fin=true;
                    }
                    if(fin!=true)
                    {
                        if(boton[y][col].getText().equals("O"))
                        {
                            ganar2++;
                        }
                        else ganar2=0;
                        if(ganar2==4)
                        {
                            JOptionPane.showMessageDialog(this,"Gana el jugador que va con el color azul","ENHORABUENA",JOptionPane.INFORMATION_MESSAGE);
                            VolverEmpezar();
                        }
                    }
                }
                else
                {
                    ganar1=0;
                    ganar2=0;
                }
            }

            // Quien gana en vertical

            ganar1=0;
            ganar2=0;
            for(int fila=0;fila<7;fila++)
            {
                if(boton[fila][x].getText()!=null)
                {
                    if(boton[fila][x].getText().equals("X"))
                    {
                        ganar1++;
                    }
                    else ganar1=0;
                    if(ganar1==4)
                    {
                        JOptionPane.showMessageDialog(this,"Gana el jugador que va con el color rojo","ENHORABUENA",JOptionPane.INFORMATION_MESSAGE);
                        VolverEmpezar();
                        fin=true;
                    }
                    if(fin!=true)
                    {
                        if(boton[fila][x].getText().equals("O"))
                        {
                            ganar2++;
                        }
                        else ganar2=0;
                        if(ganar2==4)
                        {
                            JOptionPane.showMessageDialog(this,"Gana el jugador que va con el color azul","ENHORABUENA",JOptionPane.INFORMATION_MESSAGE);
                            VolverEmpezar();
                        }
                    }
                }
            }

            // Quien gana en Oblicuo 1º Posicion De izquierda a derecha

            ganar1=0;
            ganar2=0;
            int col=y;
            int fila=x;
            while(fila>0 && col>0)
            {
                col--;
                fila--;
            }
            while(fila<7 && col<7)
            {
                if(boton[col][fila].getText()!=null)
                {
                    if(boton[col][fila].getText().equals("X"))
                    {
                        ganar1++;
                    }
                    else ganar1=0;
                    if(ganar1==4)
                    {
                        JOptionPane.showMessageDialog(this,"Gana el jugador que va con el color rojo","ENHORABUENA",JOptionPane.INFORMATION_MESSAGE);
                        VolverEmpezar();
                        fin=true;
                    }
                    if(fin!=true)
                    {
                        if(boton[col][fila].getText().equals("O"))
                        {
                            ganar2++;
                        }

                        else ganar2=0;
                        if(ganar2==4)
                        {
                            JOptionPane.showMessageDialog(this,"Gana el jugador que va con el color azul","ENHORABUENA",JOptionPane.INFORMATION_MESSAGE);
                            VolverEmpezar();
                        }
                    }
                }
                else
                {
                    ganar1=0;
                    ganar2=0;
                }
                col++;
                fila++;
            }

            // Quien gana en oblicuo? 2º Posicion de derecha a izquierda

            ganar1=0;
            ganar2=0;
            col=y;
            fila=x;

            //Buscar posición de la esquina

            while(fila<6 && col>0)
            {
                col--;
                fila++;
            }
            while(fila>-1 && col<7 )
            {
                if(boton[col][fila].getText()!=null)
                {
                    if(boton[col][fila].getText().equals("X"))
                    {
                        ganar1++;
                    }
                    else ganar1=0;
                    if(ganar1==4)
                    {
                        JOptionPane.showMessageDialog(this,"Gana el jugador que va con el color rojo","ENHORABUENA",JOptionPane.INFORMATION_MESSAGE);
                        VolverEmpezar();
                        fin=true;
                    }
                    if(fin!=true)
                    {
                        if(boton[col][fila].getText().equals("O"))
                        {
                            ganar2++;
                        }
                        else ganar2=0;
                        if(ganar2==4)
                        {
                            JOptionPane.showMessageDialog(this,"Gana el jugador que va con el color azul","ENHORABUENA",JOptionPane.INFORMATION_MESSAGE);
                            VolverEmpezar();
                        }
                    }
                }
                else
                {
                    ganar1=0;
                    ganar2=0;
                }

                col++;
                fila--;
            }
        }

        void VolverEmpezar()
        {
            for (int fila = 1; fila < 7; fila++) {
                for (int col = 0; col < 7; col++) {
                    boton[fila][col].setBackground(Color.WHITE);
                    boton[fila][col].setForeground(Color.BLACK);
                    boton[fila][col].setText(null);
                }
        }
        turno = 0;
    }

        public void actionPerformed(ActionEvent ae)
        {
            dosJugadores=true;

            if(ae.getSource()==Salir)
            {
                dispose();
            }
            if(ae.getSource()==Nuevo)
            {
                dosJugadores=true;
                VolverEmpezar();
            }
            int x=0;
            int y=0;
            for(int fila=0;fila<7;fila++)
            {
                for(int col=0;col<7;col++)
                {
                    if (ae.getSource()==boton[fila][col])
                    {
                        //Ir hasta la ultima posicion

                        int pos=7;
                        do
                        {
                            pos--;
                        }
                        while(boton[pos][col].getText()!=null & pos!=0);

                        //Pintar Ficha

                        //System.out.println("El texto del boton situado en la fila: " + (pos+1) + " y columna: " + (col+1) + " es: _" + boton[pos][col].getText() + "_");

                        if(boton[pos][col].getText()==null)
                        {
                            //System.out.println("ENTREEEEE PRIMEROOOOOO");

                            if(dosJugadores)
                            {
                                if(turno %2==0)
                                {
                                    boton[pos][col].setText("X");
                                    //Esto lo hago para no tener que cambiar las condiciones es decir borrar el texto
                                    boton[pos][col].setForeground(Color.red);
                                    boton[pos][col].setBackground(Color.red);
                                }
                                else
                                {
                                    boton[pos][col].setText("O");
                                    boton[pos][col].setForeground(Color.blue);
                                    //Esto lo hago para no tener que cambiar las condiciones es decir borrar el texto
                                    boton[pos][col].setBackground(Color.blue);
                                }
                                turno++;
                                x=col;
                                y=pos;

                               // System.out.println("ENTREEEEE");
                            }
                        }
                    }
                }
            }
            ganar(x,y);

            //Empate

            if(turno==42)
            {
                JOptionPane.showMessageDialog(this,"No hay un ganador, habeis empatado la partida","EMPATE",JOptionPane.INFORMATION_MESSAGE);
                VolverEmpezar();
            }
            fin=false;
        }

        public static void main (String [] args)
        {
            new CuatroEnRaya();
        }
}