package CuatroEnRaya;

import javax.swing.*;
import java.awt.*;

public class RoundButton extends JButton {
    public RoundButton() {
        setPreferredSize(new Dimension(60, 60));
        setContentAreaFilled(false);
    }

    public void paintComponent(Graphics g) {
        if (getModel().isArmed()) {
            g.setColor(Color.gray);
        } else {
            g.setColor(getBackground());
        }
        g.fillOval(0, 0, getSize().width - 1, getSize().height - 1);

        super.paintComponent(g);
    }

    public void paintBorder(Graphics g) {
        g.setColor(getForeground());
        g.drawOval(0, 0, getSize().width - 1, getSize().height - 1);
    }
}
